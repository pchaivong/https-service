package com.itmx.poc.httpsservice;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class DefaultResponseDTO {

    @JsonProperty(value = "status")
    private String status;
}
