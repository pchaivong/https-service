package com.itmx.poc.httpsservice;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DefaultController {

    @RequestMapping(value = "/api/fundin", method = RequestMethod.GET)
    public DefaultResponseDTO mockFundIn(){
        return new DefaultResponseDTO("success");
    }
}
